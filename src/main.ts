import { ConvertKit } from "./convertkit";
import { getScriptProperty, ScriptPropertyKey } from "./scriptProperty";

function getNumOfSubscribers() {
  const cache = CacheService.getScriptCache();
  const cached = cache.get("num_of_subscribers");
  //   If there is a cached value, return this instead of calling CK-api
  if (cached != null) {
    Logger.log(cached);
    return cached;
  }

  const apiSecret = getScriptProperty(ScriptPropertyKey.ckSecretKey);
  const convertkit = new ConvertKit("", apiSecret);
  const numOfSubscribers = convertkit.getNumOfSubscribers();
  cache.put("num_of_subscribers", numOfSubscribers.toString(), 3600);
  Logger.log(numOfSubscribers);
  return numOfSubscribers;
}

function doGet(e: GoogleAppsScript.Events.DoGet) {
  return ContentService.createTextOutput(
    JSON.stringify({ numOfSubscribers: getNumOfSubscribers() })
  );
}
