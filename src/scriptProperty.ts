export function getScriptProperty(key: ScriptPropertyKey) {
  let scriptProperties = PropertiesService.getScriptProperties();
  let val = scriptProperties.getProperty(key);
  if (val === null || val === 'ERROR') {
    scriptProperties.setProperty(key, 'ERROR');
    throw new Error(`Could not retrieve script property for key: ${key}`);
  }
  return val;
}

export function setScriptProperty(key: ScriptPropertyKey, val: any) {
  let scriptProperties = PropertiesService.getScriptProperties();
  scriptProperties.setProperty(key, val);
}

export enum ScriptPropertyKey {
  sha256Key = 'SHA256 KEY',
  sheetId = 'SHEET ID',
  ckPublicKey = 'CK PUBLIC KEY',
  ckSecretKey = 'CK SECRET KEY',
  stripeSecretKey = 'STRIPE SECRET KEY',
  stripePublicKey = 'STRIPE PUBLIC KEY',
  memberShipPrice = 'MEMBERSHIP PRICE',
  invoiceUrl = 'INVOICE URL',
  dayLimit = 'DAY LIMIT FOR REMOVAL',
  testMode = 'TEST MODE'
}
