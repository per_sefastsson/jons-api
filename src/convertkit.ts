const v3url = "https://api.convertkit.com/v3/";

interface CkPayload {
  email?: string;
  api_key?: string;
  tag?: {};
  fields?: {};
  tags?: string[];
  email_address?: string;
  api_secret?: string;
  label?: string;
}

type ConvertKitId = number;

export class ConvertKit {
  private apiKey: string;
  private secretApiKey: string;
  private tags: {};
  private forms: {};
  private fields: {};

  constructor(apiKey: string, secretApiKey: string) {
    this.apiKey = apiKey;
    this.secretApiKey = secretApiKey;
    // // get list of tags
    // this.tags = this.getTags();

    // // get forms
    // this.forms = this.getForms();

    // // get fields
    // this.fields = this.getFields();
  }

  // TAGS
  getTags(): {} {
    let endpoint: string = `tags?api_key=${this.apiKey}`;
    let result = http(endpoint);

    if (result.success) {
      let tags = {};
      result.content.tags.forEach(tag => {
        tags[tag.name] = tag.id;
      });
      return tags;
    }
    return null;
  }

  createTag(tagName: string): string {
    if (tagName in this.tags) return this.tags[tagName];

    let endpoint: string = "tags";
    let payload: CkPayload = {
      api_key: this.apiKey,
      tag: {
        name: tagName
      }
    };
    let result = http(endpoint, "post", payload);
    if (result.success) {
      let id = result.content.id;
      this.tags[tagName] = id;
      return id;
    }
    return null;
  }

  tag(email: string, tagName: string): boolean {
    let tagID = this.tags[tagName]
      ? this.tags[tagName]
      : this.createTag(tagName);
    let endpoint = `tags/${tagID}/subscribe`;
    let payload: CkPayload = {
      api_key: this.apiKey,
      email: email
    };

    let result = http(endpoint, "post", payload);
    if (result.success) {
      return true;
    }
    return false;
  }

  untag(email: string, tagName: string): boolean {
    if (!(tagName in this.tags)) return false;
    let id = this.getSubscriberId(email);
    if (!id) return false;
    let endpoint = `tags/${this.tags[tagName]}/unsubscribe`;
    let payload: CkPayload = {
      api_secret: this.secretApiKey,
      email: email
    };
    let result = http(endpoint, "post", payload);
    return result.success;
  }

  // FORMS
  getForms(): {} {
    let endpoint = `forms?api_key=${this.apiKey}`;
    let result = http(endpoint);

    if (result.success) {
      let forms = {};
      result.content.forms.forEach(form => {
        forms[form.name] = form.id;
      });
      return forms;
    }
    return null;
  }

  addSubscriber(
    email: string,
    fields: {},
    tagNames: string[],
    formName: string
  ): ConvertKitId {
    const formId = this.forms[formName];

    const endpoint = `forms/${formId}/subscribe`;

    let tagIds = [];
    tagNames.forEach(tagName => {
      if (!(tagName in this.tags)) {
        tagIds.push(this.createTag(tagName));
      } else {
        tagIds.push(this.tags[tagName]);
      }
    });

    let payload: CkPayload = {
      api_key: this.apiKey,
      email: email,
      fields: fields,
      tags: tagIds
    };

    let result = http(endpoint, "post", payload);
    if (result.success) {
      return result.content.subscription.subscriber.id;
    }
    return null;
  }

  updateSubscriber(email: string, newEmail: string, fields?: {}): boolean {
    if (email === newEmail && !fields) {
      return true;
    }
    let payload: CkPayload = {
      api_secret: this.secretApiKey
    };
    if (email !== newEmail) {
      payload.email_address = newEmail;
    }
    if (fields) {
      payload.fields = fields;
    }
    let subscriberId = this.getSubscriberId(email);
    if (!subscriberId) return false;

    let endpoint = `subscribers/${subscriberId}`;

    let result = http(endpoint, "put", payload);

    if (result.success) {
      return true;
    }
    return false;
  }

  getSubscriberId(email: string): string {
    const endpoint = "subscribers";
    const payload: CkPayload = {
      api_secret: this.secretApiKey,
      email_address: email
    };
    let result = http(endpoint, "get", payload);
    if (result.success) {
      return result.content.subscribers[0].id;
    }
    return null;
  }

  getFields(): {} {
    const endpoint = "custom_fields";
    const payload: CkPayload = {
      api_key: this.apiKey
    };
    let result = http(endpoint, "get", payload);
    if (result.responseCode === 201 || result.responseCode === 200) {
      let fields = {};
      result.content.custom_fields.forEach(field => {
        fields[field.label] = field.key;
      });
      return fields;
    }
    return null;
  }

  createField(label: string): string {
    const endpoint = "custom_fields";
    const payload: CkPayload = {
      api_secret: this.secretApiKey,
      label: label
    };
    let result = http(endpoint, "get", payload);
    if (result.success) {
    }
    return null;
  }

  getNumOfSubscribers(): number {
    const endpoint = "subscribers";
    const payload: CkPayload = {
      api_secret: this.secretApiKey
    };
    let result = http(endpoint, "get", payload);
    if (result.success) {
      return result.content.total_subscribers;
    }
    return 0;
  }
}

// Success returns 200 or 201, Fail 400 - 500

function http(
  endpoint: string,
  method: GoogleAppsScript.URL_Fetch.HttpMethod = "get",
  payload: CkPayload = null
) {
  let url = v3url + endpoint;
  let options: GoogleAppsScript.URL_Fetch.URLFetchRequestOptions = {
    method: method,
    contentType: "application/json; charset=utf-8",
    muteHttpExceptions: true
  };
  if (payload && (method === "post" || method === "put")) {
    options.payload = JSON.stringify(payload);
  } else if (payload && (method === "get" || method === "delete")) {
    url += "?";
    for (let param in payload) {
      url += param + "=" + payload[param] + "&";
    }
    url = url.slice(0, -1);
  }

  Logger.log(
    "\r\n\r\nSent Payload: " + JSON.stringify(options, 0, 2) + " to url: " + url
  );
  let httpResult = UrlFetchApp.fetch(url, options);
  let result = {
    content: JSON.parse(httpResult.getContentText()),
    responseCode: httpResult.getResponseCode(),
    success: false
  };
  result["success"] =
    result.responseCode == 200 || result.responseCode == 201 ? true : false;
  Logger.log("Recieved result: " + JSON.stringify(result.content, 0, 2));
  return result;
}
