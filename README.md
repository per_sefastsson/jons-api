# Läs mig

För att undvika för många API-anrop från google drive så är antalet prenumeranter cachat, så det uppdateras som mest bara en gång i timmen. Detta är för att det tar tid (och det är väl knappast nödvändigt att i realtid visa antalet), samt att google har kvoter för hur många api-anrop man får göra.

Vill du ändra hur ofta detta ska uppdateras så letar du reda på raden

```
cache.put("num_of_subscribers", numOfSubscribers.toString(), 3600);
```

och ändrar siffran till något annat (det är i sekunder, så 3600 är en timme)

## Installera dependencies

Kör:

```
npm install
```

för att installera det du behöver för att kunna pusha direkt till ditt google drive

Innan du kan köra något behöver du köra kommandot:

```
npx clasp login
```

Logga där in med det googlekonto där du ska lägga ditt skript

## Ställ in dina google scriptsinställningar

### 1. Skapa ett script någonstans på din google drive

Det här kan skapas var som helst, du kan flytta filen sen, det spelar ingen roll. Kopiera ID:et (den hashade delen av URL:en).

### 2. Lägg in script-id:et i .clasp.json

### 3. Kör npm run deploy

När du kör

```
npm run deploy
```

så pushas källkoden till ditt script.

### 4. Ställ in wepp-Api i ditt google script

När du har din källkod pushad så går du in i menyn publicera -> webbapp

Där ställer du in att den ska exekveras som dig, men tillgänglig för vem som helst.

### 5. Ställ in din CK-API-nyckel

Under menyn Arkiv->Projektegenskaper->scriptegenskaper sätter du en ny egenskap med namnet

```
CK SECRET KEY
```

med din hemliga API-nyckel.

### 6. hämta datan från ditt API på din sida

```
fetch("https://script.google.com/macros/s/AKfycbwHq5_aeK39AYFdsOghEu2s8Jac2GScVYkVQJoEs2ZewcNg7tAB/exec")
      .then(res => res.json())
      .then(json => {
         let numOfSubscribers = json.numOfSubscribers;
         // sätt något element till numOfSubscriber för att visa antalet prenumeranter
        } else {
          // Hanera om något gick snett
        }
      });
```
